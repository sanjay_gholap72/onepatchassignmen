package com.example.onepatchassignment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditViewData extends AppCompatActivity
{

    DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_view_data);

        mydb = new DBHelper(this);

        Intent intentPage = getIntent();
        String studID = intentPage.getStringExtra("EmpId");

        final EditText editStudID = (EditText)findViewById(R.id.editStudID);
        editStudID.setFocusable(false); // disable input
        final EditText editName = (EditText)findViewById(R.id.editName);
        final EditText editAddress = (EditText)findViewById(R.id.editAddress);
        final EditText editProgram = (EditText)findViewById(R.id.editProgram);
        final EditText editPhone = (EditText)findViewById(R.id.editPhone);
        final EditText editEmail = (EditText)findViewById(R.id.editEmail);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdate);
        Button btnDelete = (Button)findViewById(R.id.btnDelete);

        Cursor cursor = mydb.searchData(studID);
        cursor.moveToFirst();

        if(cursor.getCount() < 1)
        {
            // show toast message
            Toast.makeText(getApplicationContext(), "No data found",Toast.LENGTH_SHORT).show();
        }
        else
        {
            @SuppressLint("Range") String id = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_ID)));
            @SuppressLint("Range")  String name = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_NAME));
            @SuppressLint("Range")  String address = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_ADDRESS));
            @SuppressLint("Range") String program = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_PROGRAM));
            @SuppressLint("Range") String phone = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_PHONE));
            @SuppressLint("Range") String email = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_EMAIL));

            editStudID.setText(id);
            editName.setText(name);
            editAddress.setText(address);
            editProgram.setText(program);
            editPhone.setText(phone);
            editEmail.setText(email);

            Toast.makeText(getApplicationContext(), "Data found",Toast.LENGTH_SHORT).show();
        }


        if (!cursor.isClosed())
        {
            cursor.close();
        }



        btnUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                final String id = editStudID.getText().toString();
                final String names = editName.getText().toString();
                final String addr = editAddress.getText().toString();
                final String prog = editProgram.getText().toString();
                final String phones = editPhone.getText().toString();
                final String emails = editEmail.getText().toString();


                AlertDialog.Builder alert = new AlertDialog.Builder(EditViewData.this);
                alert.setTitle("Update");
                alert.setMessage("Are you sure you want to update data?");

                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        if(mydb.updateData(id,names,addr,prog,phones,emails))
                        {
                            Toast.makeText(getApplicationContext(), "Data updated successfully",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });

                alert.setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });

                alert.show(); // show alert message
            }
        });


        btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String id = editStudID.getText().toString(); // get id

                AlertDialog.Builder alert = new AlertDialog.Builder(EditViewData.this);
                alert.setTitle("Delete");
                alert.setMessage("Are you sure you want to delete?");

                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if(mydb.deleteData(id))
                        {
                            Toast.makeText(getApplicationContext(), "Data deleted successfully",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });

                alert.setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
                alert.show(); // show alert message
            }
        });
    }
}
