package com.example.onepatchassignment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnInsertDataPage = (Button)findViewById(R.id.btnInsertDataPage);
        Button btnViewDataPage = (Button)findViewById(R.id.btnViewDataPage);
        Button btnSearchDataPage = (Button)findViewById(R.id.btnSearchDataPage);

        btnInsertDataPage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent intentPage = new Intent(MainActivity.this,InsertData.class);
                startActivity(intentPage);
            }
        });


        btnViewDataPage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intentPage = new Intent(MainActivity.this, ViewData.class);
                startActivity(intentPage);
            }
        });


        btnSearchDataPage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intentPage = new Intent(MainActivity.this, SearchData.class);
                startActivity(intentPage);
            }
        });
    }
}
