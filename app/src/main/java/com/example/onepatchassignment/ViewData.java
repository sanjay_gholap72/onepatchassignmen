package com.example.onepatchassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class ViewData extends AppCompatActivity
{

    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);


        mydb = new DBHelper(this);

        Cursor cursor = mydb.listAllData(); // get data from db
        ListView listData = (ListView) findViewById(R.id.listData);
        ListDataCursorAdapter listdataAdapter = new ListDataCursorAdapter(this, cursor);
        listData.setAdapter(listdataAdapter);


        listData.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                TextView sid = (TextView)view.findViewById(R.id.display_studid); //the 'view' refers to current position of the item
                String studID = sid.getText().toString();
                Intent intentPage = new Intent(ViewData.this, SearchData.class);
                intentPage.putExtra("EmpId",studID);
                startActivity(intentPage);
            }
        });
    }
}
