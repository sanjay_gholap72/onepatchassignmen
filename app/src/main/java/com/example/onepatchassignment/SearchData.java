package com.example.onepatchassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class SearchData extends AppCompatActivity
{
    DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_data);

        mydb = new DBHelper(this);

        Intent intentPage = getIntent();
        String intstudID = intentPage.getStringExtra("EmpId");

        final EditText sid = (EditText)findViewById(R.id.editSearch);
        ImageButton btnSearch = (ImageButton)findViewById(R.id.btnSearch);
        final ImageButton btnCall = (ImageButton)findViewById(R.id.btnCall);
        final ImageButton btnMsg = (ImageButton)findViewById(R.id.btnMsg);
        final ImageButton btnEmail = (ImageButton)findViewById(R.id.btnEmail);
        final ImageButton btnMap = (ImageButton)findViewById(R.id.btnMap);
        final ImageButton btnEdit = (ImageButton)findViewById(R.id.btnEdit);
        final TextView resID = (TextView)findViewById(R.id.resStudID);
        final TextView resName = (TextView)findViewById(R.id.resName);
        final TextView resAddress = (TextView)findViewById(R.id.resAddress);
        final TextView resProgram = (TextView)findViewById(R.id.resProgram);
        final TextView resPhone = (TextView)findViewById(R.id.resPhone);
        final TextView resEmail = (TextView)findViewById(R.id.resEmail);

        btnEdit.setVisibility(View.GONE);
        btnCall.setVisibility(View.GONE);
        btnMsg.setVisibility(View.GONE);
        btnEmail.setVisibility(View.GONE);
        btnMap.setVisibility(View.GONE);


        if(intstudID != null)
        {

            sid.setVisibility(View.GONE);
            btnSearch.setVisibility(View.GONE);

            Cursor cursor = mydb.searchData(intstudID);
            cursor.moveToFirst();

            @SuppressLint("Range") String id = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_ID)));
            @SuppressLint("Range") String name = String.valueOf((cursor.getColumnIndex(DBHelper.COLUMN_NAME)));
            @SuppressLint("Range") String address = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_ADDRESS)));
            @SuppressLint("Range") String program = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_PROGRAM)));
            @SuppressLint("Range") String phone = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_PHONE)));
            @SuppressLint("Range") String email = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_EMAIL)));

            resID.setText(id);
            resName.setText(name);
            resAddress.setText(address);
            resProgram.setText(program);
            resPhone.setText(phone);
            resEmail.setText(email);

            btnEdit.setVisibility(View.VISIBLE);
            btnCall.setVisibility(View.VISIBLE);
            btnMsg.setVisibility(View.VISIBLE);
            btnEmail.setVisibility(View.VISIBLE);
            btnMap.setVisibility(View.VISIBLE);


            Toast.makeText(getApplicationContext(), "Data found",Toast.LENGTH_SHORT).show();


            if (!cursor.isClosed())
            {
                cursor.close();
            }
        }


        btnSearch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String studid = sid.getText().toString();

                Cursor cursor = mydb.searchData(studid);
                cursor.moveToFirst();

                if(cursor.getCount() < 1)
                {
                    resID.setText("");
                    resName.setText("");
                    resAddress.setText("");
                    resProgram.setText("");
                    resPhone.setText("");
                    resEmail.setText("");

                    btnEdit.setVisibility(View.GONE);
                    btnCall.setVisibility(View.GONE);
                    btnMsg.setVisibility(View.GONE);
                    btnEmail.setVisibility(View.GONE);
                    btnMap.setVisibility(View.GONE);

                    Toast.makeText(getApplicationContext(), "No data found",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    @SuppressLint("Range")  String id = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_ID)));
                    @SuppressLint("Range")    String name = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_NAME)));
                    @SuppressLint("Range")  String address = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_ADDRESS)));
                    @SuppressLint("Range")   String program = cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_PROGRAM)));
                    @SuppressLint("Range") String phone =cursor.getString ((cursor.getColumnIndex(DBHelper.COLUMN_PHONE)));
                    @SuppressLint("Range") String email =cursor.getString((cursor.getColumnIndex(DBHelper.COLUMN_EMAIL)));

                    resID.setText(id);
                    resName.setText(name);
                    resAddress.setText(address);
                    resProgram.setText(program);
                    resPhone.setText(phone);
                    resEmail.setText(email);

                    btnEdit.setVisibility(View.VISIBLE);
                    btnCall.setVisibility(View.VISIBLE);
                    btnMsg.setVisibility(View.VISIBLE);
                    btnEmail.setVisibility(View.VISIBLE);
                    btnMap.setVisibility(View.VISIBLE);


                    Toast.makeText(getApplicationContext(), "Data found",Toast.LENGTH_SHORT).show();
                }

                if (!cursor.isClosed())
                {
                    cursor.close();
                }
            }
        });


        btnEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String resStudID = resID.getText().toString();

                if(resStudID != "")
                {
                    Intent intentPage = new Intent(SearchData.this, EditViewData.class);
                    intentPage.putExtra("EmpId",resStudID);
                    startActivity(intentPage);
                }
                else
                {

                    Toast.makeText(getApplicationContext(), "No data to edit",Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnCall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intentPage = new Intent(Intent.ACTION_DIAL);
                intentPage.setData(Uri.parse("tel:"+resPhone.getText().toString()));
                startActivity(intentPage);
            }
        });


        btnMsg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri uri = Uri.parse("smsto:" + resPhone.getText().toString());
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(Intent.createChooser(i, ""));
            }
        });


        btnEmail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri uri = Uri.parse("mailto:" + resEmail.getText().toString());
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(Intent.createChooser(i, ""));
            }
        });


        btnMap.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+resAddress.getText().toString());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

    }
}
