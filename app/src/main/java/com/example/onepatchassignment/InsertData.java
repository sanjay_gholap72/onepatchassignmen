package com.example.onepatchassignment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertData extends AppCompatActivity
{
    DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_data);

        mydb = new DBHelper(this);

        final EditText studID = (EditText)findViewById(R.id.editStudID);
        final EditText name = (EditText)findViewById(R.id.editName);
        final EditText address = (EditText)findViewById(R.id.editAddress);
        final EditText program = (EditText)findViewById(R.id.editProgram);
        final EditText phone = (EditText)findViewById(R.id.editPhone);
        final EditText email = (EditText)findViewById(R.id.editEmail);
        Button btnClear = (Button)findViewById(R.id.btnClear);
        Button btnSave = (Button)findViewById(R.id.btnSave);

        btnClear.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                studID.setText("");
                name.setText("");
                address.setText("");
                program.setText("");
                phone.setText("");
                email.setText("");


                Toast.makeText(getApplicationContext(), "Cleared", Toast.LENGTH_SHORT).show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String id = studID.getText().toString();
                final String names = name.getText().toString();
                final String addr = address.getText().toString();
                final String prog = program.getText().toString();
                final String phones = phone.getText().toString();
                final String emails = email.getText().toString();

                AlertDialog.Builder alert = new AlertDialog.Builder(InsertData.this);
                alert.setTitle("Insert");
                alert.setMessage("Are you sure you want to insert data?");

                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if(!checkIsEmpty())
                        {
                            if (mydb.insertData(id, names,addr, prog, phones, emails))
                            {
                                Toast.makeText(getApplicationContext(), "Data save successfully", Toast.LENGTH_SHORT).show();

                                studID.setText("");
                                name.setText("");
                                address.setText("");
                                program.setText("");
                                phone.setText("");
                                email.setText("");
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });

                alert.setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });

                alert.show(); // show alert message
            }


            public boolean checkIsEmpty()
            {
                boolean res = false;
                if(TextUtils.isEmpty(studID.getText()))
                {
                    studID.setError("Student RegNo cannot be empty.");
                    res = true;
                }
                if(TextUtils.isEmpty(name.getText()))
                {
                    name.setError("Name cannot be empty.");
                    res = true;
                }
                if(TextUtils.isEmpty(address.getText()))
                {
                    address.setError("Address cannot be empty.");
                    res = true;
                }
                if(TextUtils.isEmpty(program.getText()))
                {
                    program.setError("Department cannot be empty.");
                    res = true;
                }
                if(TextUtils.isEmpty(phone.getText()))
                {
                    phone.setError("Phone number cannot be empty.");
                    res = true;
                }
                if(TextUtils.isEmpty(email.getText()))
                {
                    email.setError("Email address cannot be empty.");
                    res = true;
                }

                return res;
            }
        });
    }
}
